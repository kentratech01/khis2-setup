#!/bin/sh

find /home/kafka/kafka/logs/ -maxdepth 1 -regextype egrep  -regex ".*/[a-z\-]+.log.[0-9]{4}-[0-9]{2}-[0-9]{2}-[0-9]{2}$" -delete