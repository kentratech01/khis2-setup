#!/bin/bash
##################################################################################
##                 								##
## Kentra Technologies®                                                       	##
##   										##
## KHIS2 Bootstrapping installation						##
##										##
##################################################################################

function khisstartup {
	sudo apt-get update  
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error running APTITUDE update ,,,"
        	exit
	fi

	sudo apt-get upgrade -y 
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error running APTITUDE upgrade ,,,"
		exit
	fi

	sudo apt-get install -y dialog
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing DIALOG ,,,"
        	exit
	fi
}

function postgres96 {
	tput setaf 6 ; echo "OK, this KHIS Application Server will connect to a local Postgres9.6 Database ..."
        tput sgr0

	# Create the file repository configuration:
	sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error creating postgres-9.6 repository configuration  ,,,"
                exit
        fi

	# Import the repository signing key:
	wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error importing postgres-9.6 repository signing key  ,,,"
                exit
        fi

	sudo apt-get update

	sudo apt-get install -y postgresql-9.6
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing POSTGRES-9.6 ,,,"
                exit
        fi

	sudo apt-get install -y postgresql-contrib-9.6
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing POSTGRES-CONTRIB-9.6 ,,,"
                exit
        fi

        sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'PostgreSQL@k3ntr4';"
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error changing postgres password ,,,"
                exit
        fi

        sudo cat config/templates/config_files/pg_hba.conf > change.txt && sudo mv change.txt /etc/postgresql/9.6/main/pg_hba.conf
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error changing pg_hba.conf ,,,"
                exit
        fi

	sudo sed -i /etc/postgresql/9.6/main/postgresql.conf -e "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" -e "s/max_connections = 100\t/max_connections = 1000/"
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error changing postgresql.conf ,,,"
                exit
        fi

        sudo systemctl restart postgresql
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error restarting POSTGRES ,,,"
                exit
        fi

        HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Python version to run postgres"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Python"
        	 2 "Python3")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	2)
            		sudo apt-get install postgresql-plpython3-9.6
            		postgrespy3;
            		;;
        	1)
			sudo cp config/templates/packages/postgresql9.6_plpython.zip .

			sudo unzip postgresql9.6_plpython.zip

			sudo cp -r postgresql9.6_plpython/extension/* /usr/share/postgresql/9.6/extension/

			sudo cp -r postgresql9.6_plpython/lib/* /usr/lib/postgresql/9.6/lib/
            		
            		postgrespy;
            		;;
	esac
}

function postgres12 {

	tput setaf 6 ; echo "OK, this KHIS Application Server will connect to a local Postgres12 Database ..."
        tput sgr0

        # Create the file repository configuration:
	sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error creating postgres-12.0 repository configuration  ,,,"
                exit
        fi

	# Import the repository signing key:
	wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error importing postgres-12.0 repository signing key  ,,,"
                exit
        fi

        sudo apt-get install -y postgresql-12
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing POSTGRES12 ,,,"
                exit
        fi

        sudo apt-get install -y postgresql-client-12
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing POSTGRES-CLIENT-12 ,,,"
                exit
        fi

        sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'PostgreSQL@k3ntr4';"
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error changing postgres password ,,,"
                exit
        fi

        sudo cat config/templates/config_files/pg_hba.conf > change.txt && sudo mv change.txt /etc/postgresql/12/main/pg_hba.conf
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error changing pg_hba.conf ,,,"
                exit
        fi

       	sudo sed -i /etc/postgresql/12/main/postgresql.conf -e "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" -e "s/max_connections = 100\t/max_connections = 1000/"
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error changing postgresql.conf ,,,"
                exit
        fi

        sudo systemctl restart postgresql
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error restarting POSTGRES ,,,"
                exit
        fi

        sudo apt-get install -y postgresql-plpython3-12

        postgrespy3;

}

function postgrespy3 {

	psql -d postgres -U postgres -f config/templates/dmls/postgres_py3.dml
        psql -d infomed -U postgres -f config/templates/dmls/infomed.dml
        psql -d khis_logs -U postgres -f config/templates/dmls/khis_logs.dml
        psql -d auth -U postgres -f config/templates/dmls/auth.dml
        psql -d khis -U postgres -f config/templates/dmls/khis_extensions_py3.dml
        

        HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Khis Database Installation"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Install Khis Database"
        	 2 "Skip Khis Database Installation")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	1)
            		psql -d khis -U postgres -f config/templates/dmls/khis.sql
        		psql -d khis -U postgres -f config/templates/dmls/khis_user_py3.dml
            		;;
        	2)
			tput setaf 6 ; echo "Skipping Khis bd installation ..."
        		tput sgr0
            		;;
	esac
}

function postgrespy {

	psql -d postgres -U postgres -f config/templates/dmls/postgres_py.dml
	psql -d infomed -U postgres -f config/templates/dmls/infomed.dml
	psql -d khis_logs -U postgres -f config/templates/dmls/khis_logs.dml
	psql -d auth -U postgres -f config/templates/dmls/auth.dml
	psql -d khis -U postgres -f config/templates/dmls/khis_extensions_py.dml

	HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Khis Database Installation"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Install Khis Database"
        	 2 "Skip Khis Database Installation")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	1)
            		psql -d khis -U postgres -f config/templates/dmls/khis.sql
			psql -d khis -U postgres -f config/templates/dmls/khis_user_py.dml
            		;;
        	2)
			tput setaf 6 ; echo "Skipping Khis bd installation ..."
        		tput sgr0
            		;;
	esac

	# PostgreSQL@k3ntr4
}

function nopostgres {
        tput setaf 6 ; echo "OK, this KHIS Application Server will connect to a remote Database Servern"
	tput sgr0
	
	# Install Postgres dev
	sudo apt-get install -y postgresql-client
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing POSTGRES-CLIENT ,,,"
        	exit
	fi
}

function dbselection {
        HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Database version"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Postgresql 9.6"
        	 2 "Postgresql 12"
         	 3 "No database Server on this KHIS ")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	1)
            		postgres96;
            		;;
        	2)
            		postgres12;
            		;;
        	3)
			nopostgres;
            		;;
	esac

}

function pythonselection {
        HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Python version"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Python 2"
        	 2 "Python 3"
		 3 "Both - Default (Python3) "
		 4 "Skip Python instalation ")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	1)
			khispython2;
            		;;
        	2)
			khispython3;
            		;;
        	3)
			khispython2;
			khispython3;
            		;;
		4)
			tput setaf 6 ; echo "Skipping python installation ..."
        		tput sgr0
            		;;
	esac

}

function khisbootstrap {
	# Install KHIS Ubuntu Bootstrap

        sudo  apt-get install -y gnupg2
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing GNUPG2 ,,,"
                exit
        fi

       sudo apt install net-tools
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing NET-TOOLS ,,,"
                exit
        fi

	sudo apt-get install -y nginx 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing NGINX ,,,"
		exit
	fi

	sudo cat config/templates/config_files/sites_avaliable_default_config > change.txt && sudo mv change.txt /etc/nginx/sites-available/default
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error changing sites-available ,,,"
                exit
        fi

        sudo service nginx restart

	sudo apt-get install -y pdftk-java
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing PDFTK ,,,"
		exit
	fi

	sudo apt-get install -y git-all 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing GIT-ALL ,,,"
		exit
	fi

	sudo apt-get install -y docker.io
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing iDOCKER.IO ,,,"
		exit
	fi

	sudo apt-get install -y docker-compose
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing DOCKER-COMPOSE ,,,"
		exit
	fi

	sudo apt-get install -y cups 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing CUPS ,,,"
		exit
	fi

	sudo apt-get install -y libcups2-dev
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing LIBCUPS2-DEV ,,,"
		exit
	fi

	sudo apt-get install -y libcups2 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing LIBCUPS2 ,,,"
		exit
	fi

	sudo apt-get install -y unzip  
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing UNZIP ,,,"
		exit
	fi

	wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing NVM ,,,"
		exit
	fi

	export NVM_DIR="$HOME/.nvm" && [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" bash_completion

	nvm install 14.17.0
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing NODE.JS and NPM,,,"
		exit
	fi

	npm install --global yarn   
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing YARN ,,,"
		exit
	fi

	sudo apt install -y software-properties-common
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing SOFTWARE-PROPERTIES-COMMON ,,,"
        	exit
	fi
}	

function khiajava {
	# Install JAVA versions
	sudo apt-get install -y openjdk-8-jre  &&
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing OPENJDK-8-JRE ,,,"
		exit
	fi

	sudo apt-get install -y openjdk-8-jdk  &&
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing OPENJDK-8-JDK ,,,"
		exit
	fi

	sudo apt-get install -y default-jdk  &&
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing DEFAULT-JDK ,,,"
		exit
	fi

	sudo apt-get install -y openjdk-17-jdk  &&
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing OPENJDK-17-JDK ,,,"
		exit
	fi

	sudo wget https://dlcdn.apache.org/maven/maven-3/3.8.8/binaries/apache-maven-3.8.8-bin.zip
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error downloading Maven ,,,"
		exit
	fi

	sudo unzip apache-maven-3.8.8-bin.zip
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error unziping Maven ,,,"
		exit
	fi

	sudo rm -r apache-maven-3.8.8-bin.zip

	sudo mv apache-maven-3.8.8 ~

	tput setaf 6; java -version
	tput setaf 6; javac -version
	tput sgr0	

}

function pythonrequirements {
	# Install required packages for Python
	sudo apt install -y build-essential 
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing BUILD-ESSENTIAL ,,,"
        	exit
	fi

	sudo apt install -y zlib1g-dev 
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing ZLIBBLG-DWV ,,,"
        	exit
	fi

	sudo apt install -y libncurses5-dev 
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing LIBNCURSES5-DEV ,,,"
        	exit
	fi

	sudo apt install -y libgdbm-dev 
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing LIBQDBM-DEV ,,,"
        	exit
	fi

	sudo apt install -y libnss3-dev 
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing LIBNSS3-DEV ,,,"
        	exit
	fi

	sudo apt install -y libssl-dev 
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing LIBSSL-DEV ,,,"
        	exit
	fi

	sudo apt install -y libreadline-dev 
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing LIBREADLINE-DEV ,,,"
        	exit
	fi

	sudo apt install -y libffi-dev
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing LIBFFI-DEV ,,,"
        	exit
	fi

	sudo apt-get install -y libpq5 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing LIBQ5 ,,,"
		exit
	fi

	sudo apt-get install -y libpq-dev
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing LIBQ-DEV ,,,"
		exit
	fi
}

function khisrvmruby {
	# Install RVM & Ruby
	curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -  
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error importing MPAPIS.ASC certificate ,,,"
		exit
	fi

	curl -sSL https://rvm.io/pkuczynski.asc | gpg2 --import -   
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error inporting PKUCZYNSKI.ASC certificateK ,,,"
		exit
	fi

	curl -sSL https://get.rvm.io | bash -s stable  
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing RVM ,,,"
		exit
	fi

	source /home/kentra/.rvm/scripts/rvm  

	rvm install 3.0.1 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing RUBY ,,,"
		exit
	fi

	rvm --default use 3.0.1 
	tput setaf 6; rvm --version
	tput setaf 6; ruby --version
	tput sgr0


	# Install Gems
	gem install bundler 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing BUNDLER ,,,"
		exit
	fi

	gem install rails -v 6.1.4.4   
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing GEM RAILS ,,,"
		exit
	fi

	gem install pg 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing GEM PG ,,,"
		exit
	fi

	tput setaf 6; rails -v
	tput sgr0

}

function khissidekiq {
	# Install Redis & Sidekiq
	sudo apt-get install -y redis-server 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing REDIS-SERVER ,,,"
		exit
	fi

	sed -i "s/supervised no/supervised systemd/" /etc/redis/redis.conf

	sudo systemctl restart redis.service

	sudo apt-get install -y ruby-sidekiq  
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing SIDEKIQ ,,,"
		exit
	fi
}

function khispython3 {
	# Install Python 3
	sudo apt-get install -y python3.8 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing PHYNTON ,,,"
		exit
	fi

	sudo apt-get install -y python3-dev  
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing PHYNTON-DEV ,,,"
		exit
	fi

	sudo apt-get reinstall -y python3-pip  
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing PHYNTON3-PIP ,,,"
		exit
	fi

	tput setaf 6; python3 --version
	tput setaf 6; pip3 --version
	tput sgr0

	sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 10
	sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 20
	if [[ $? != 0 ]]
        then
		tput setaf 1; echo "Error seting up default Python to Pynthon3 ,,,"
                exit
        fi

	tput setaf 6; python --version
	tput sgr0
	sleep 10

	sudo apt-get install -y python3-m2crypto
	if [[ $? != 0 ]]
	then
        	tput setaf 1; echo "Error installing Python M2CRIPTO ,,,"
        	exit
	fi

	# Install Python required libs
	sudo pip3 install suds-py3
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing Python SUDS-PY3 ,,,"
		exit
	fi

	sudo pip3 install requests
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing Python REQUESTS ,,,"
		exit
	fi

	sudo pip3 install psycopg2 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing Python PSYCOPG2 ,,,"
		exit
	fi

}

function khispython2 {
	# Install Python 2
	sudo apt-get install -y python2
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing Python2 ,,,"
                exit
        fi

	sudo apt-get install -y python-dev
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing PHYNTON-DEV ,,,"
                exit
        fi

	curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py     
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error getting GET-PIP  ,,,"
                exit
        fi 

	sudo python2 get-pip.py
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing Python PIP2 ,,,"
                exit
        fi

	rm get-pip.py

	tput setaf 6; python2 --version
        tput setaf 6; pip2 --version
        tput sgr0

	sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10
	sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 20
	if [[ $? != 0 ]]
        then
		tput setaf 1; echo "Error seting up default Python to Pynthon2 ,,,"
                exit
        fi

	tput setaf 6; python --version
	tput sgr0
	sleep 10

	sudo apt-get install -y libssl-dev
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing Python LIBSSL-DEV ,,,"
                exit
        fi

	sudo apt-get install -y swig
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing Python SWIG ,,,"
                exit
        fi

	sudo apt-get install -y gcc
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing Python GCC ,,,"
                exit
        fi

	sudo pip2 install M2Crypto
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing Python M2Cypto ,,,"
		exit
	fi

	# Install Python required libs
        sudo pip2 install suds
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing Python SUDS-PY3 ,,,"
                exit
        fi

        sudo pip2 install requests
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing Python REQUESTS ,,,"
                exit
        fi

        sudo pip2 install psycopg2
        if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error installing Python PSYCOPG2 ,,,"
                exit
        fi
}

function glassfishselection {
        HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Glassfish Installation"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Install glassfish"
        	 2 "Skip glassfish installation")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	1)
            		khisglassfish4;
            		;;
        	2)
            		tput setaf 6 ; echo "Skipping glassfish installation ..."
        		tput sgr0
            		;;
	esac

}

function khisglassfish4 {
	# Install Glassfish
	wget http://download.java.net/glassfish/4.1.2/release/glassfish-4.1.2.zip  
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error downloading GLASSFISH4 ,,,"
		exit
	fi

	unzip glassfish-4.1.2.zip  
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error unziping GLASSFISH4 ,,,"
		exit
	fi

	sudo mv glassfish4 /opt  
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error moving GLASSFISH4 to /opt ,,,"
		exit
	fi
	sudo chmod 777 -R /opt/glassfish4
	rm glassfish-4.1.2.zip

	# Configuring Glassfish
	sed '$ a AS_JAVA="/usr/lib/jvm/java-1.8.0-openjdk-amd64"' /opt/glassfish4/glassfish/config/asenv.conf > change.txt && mv change.txt /opt/glassfish4/glassfish/config/asenv.conf

	sudo cp config/templates/services/glassfish.service /etc/systemd/system/glassfish.service
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error copying GLASSFISH.SERVICE TO /etc/systemd/system/glassfish.service  ,,,"
                exit
        fi

	sudo systemctl daemon-reload
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error reloading Daemon  ,,,"
                exit
        fi

	sudo systemctl enable glassfish
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error enabling SYSTEMCTL GLASSFISH  ,,,"
                exit
        fi

	sudo systemctl start glassfish
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error starting SytemCTL GLASSFISH  ,,,"
                exit
        fi

        /opt/glassfish4/bin/asadmin --host localhost  --port 4848 change-admin-password
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error changing admin password ,,,"
                exit
        fi

        /opt/glassfish4/bin/asadmin --host localhost  --port 4848 enable-secure-admin
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error enabling secure admin  ,,,"
                exit
        fi

        sudo systemctl restart glassfish
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error restarting SytemCTL GLASSFISH  ,,,"
                exit
        fi

}

function mirthselection {
        HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Mirth Installation"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Install Mirth"
        	 2 "Skip Mirth installation")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	1)
            		khismirth;
            		;;
        	2)
            		tput setaf 6 ; echo "Skipping Mirth installation ..."
        		tput sgr0
            		;;
	esac

}

function khismirth {
	# Install Glassfish
	sudo wget http://downloads.mirthcorp.com/connect/3.11.0.b2609/mirthconnect-3.11.0.b2609-unix.tar.gz
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error downloading Mirth Connect ,,,"
		exit
	fi

	sudo tar xvfz mirthconnect-3.11.0.b2609-unix.tar.gz
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error untarring Mirth Connect ,,,"
		exit
	fi

	sudo mv Mirth\ Connect/ /opt/mirthconnect 
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error moving Mirth Connect to /opt ,,,"
		exit
	fi

	sudo chmod 777 -R /opt/mirthconnect
	sudo rm mirthconnect-3.11.0.b2609-unix.tar.gz

	PORT1="8081"
	PORT2="8443"

	resp=$(dialog --backtitle "KHIS2 Installation process" --title "KHIS2 Mirth Connect" \
	--form "\nKHIS2 Mirth Connect Configuration" 25 60 16 \
	"Mirth HTTP Port:" 1 1 $PORT1 1 25 25 30 \
	"Mirth HTTPS Port:" 2 1 $PORT2 2 25 25 30 \
	2>&1 >/dev/tty)

	i=0
	while read -r line; do
	   ((i++))
	   declare var$i="${line}"
	done <<< "${resp}"

	# Configuring Mirth Port
	sed -i -e "s/^http.port =.*/http.port = ${var1}/" -e "s/^https.port =.*/https.port = ${var2}/" /opt/mirthconnect/conf/mirth.properties

	sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/java-1.8.0-openjdk-amd64/bin/java 2000
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error updating java version  ,,,"
                exit
        fi

	sudo cp config/templates/services/mirthconnect.service /etc/systemd/system/mirthconnect.service
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error copying MIRTHCONNECT.SERVICE TO /etc/systemd/system/mirthconnect.service  ,,,"
                exit
        fi

	sudo systemctl daemon-reload
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error reloading Daemon  ,,,"
                exit
        fi

	sudo systemctl enable mirthconnect
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error enabling SYSTEMCTL MIRTHCONNECT  ,,,"
                exit
        fi

	sudo systemctl start mirthconnect
	if [[ $? != 0 ]]
        then
                tput setaf 1; echo "Error starting SytemCTL MIRTHCONNECT  ,,,"
                exit
        fi

}

function mirthselection {
        HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Mirth Installation"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Install Mirth"
        	 2 "Skip Mirth installation")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	1)
            		khismirth;
            		;;
        	2)
            		tput setaf 6 ; echo "Skipping Mirth installation ..."
        		tput sgr0
            		;;
	esac

}

function kafkaselection {
        HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Kafka Installation"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Install Kafka"
        	 2 "Skip Kafka installation")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	1)
            		khiskafka;
            		;;
        	2)
            		tput setaf 6 ; echo "Skipping Kafka installation ..."
        		tput sgr0
            		;;
	esac

}

function khiskafka {
	# Installing Kafka & Zookeeper
	
	sudo apt-get install -y zookeeper
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error installing Apache Zookeeper ..."
		exit
	fi
	
	if id "kafka" &>/dev/null; then
		tput setaf 1; echo "OK Kafka User Exists ..."	
        else 
	        sudo useradd kafka -p $(openssl passwd -1 k3ntr4) -m -G sudo -c "Kafka for KHIS2,,,,"
	        if [[ $? != 0 ]]
	        then
		   tput setaf 1; echo "Error when creating user Kafka ..."
		   exit
		fi
        fi

	#su -l kafka 
	#if [[ $? != 0 ]]
	#then
	#	tput setaf 1; echo "Error when logging on as Kafka user ..."
	#	exit
	#fi

	mkdir ~/Downloads
	curl "https://downloads.apache.org/kafka/3.3.1/kafka_2.13-3.3.1.tgz" -o ~/Downloads/kafka.tgz
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error downloading Kafka ..."
		exit
	fi	
	
	mkdir ~/kafka && cd ~/kafka
	tar -xvzf ~/Downloads/kafka.tgz --strip 1
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error extracting Kafka..."
		exit
	fi
	
	echo 'delete.topic.enable = true' >> ~/kafka/config/server.properties
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error appending delete.topic.enable to server.properties ..."
		exit
	fi
	
	sudo mkdir /var/log/kafka
	sed -i 's/log.dirs=\/tmp\/kafka-logs/log.dirs=\/var\/log\/kafka/g' ~/kafka/config/server.properties
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error creating changing log destination in Kafka server.properties ..."
		exit
	fi

	sed -i 's/#log.retention.bytes/log.retention.bytes/g' ~/kafka/config/server.properties
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error creating changing log retention in Kafka server.properties ..."
		exit
	fi


	sudo cp ~/khis2-setup/config/templates/config_files/log4j.properties ~/kafka/config/log4j.properties
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error creating changing log destination in Kafka log4j.properties ..."
		exit
	fi

	sudo cp ~/khis2-setup/config/templates/config_files/connect-log4j.properties ~/kafka/config/connect-log4j.properties
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error creating changing log destination in Kafka connect-log4j.properties ..."
		exit
	fi

	sudo mv ~/kafka /home/kafka
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error creating zookeeper.service ..."
		exit
	fi
	
	sudo cp ~/khis2-setup/config/templates/config_files/zookeeper.service /etc/systemd/system/zookeeper.service
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error creating zookeeper.service ..."
		exit
	fi
	
	sudo cp ~/khis2-setup/config/templates/config_files/kafka.service /etc/systemd/system/kafka.service
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error creating kafka.service ..."
		exit
	fi

	sudo systemctl enable zookeeper
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error system enabled zookeeper ..."
		exit
	fi
	
	sudo systemctl enable kafka
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error system enabled kafka ..."
		exit
	fi
	
	sudo systemctl start zookeeper
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error starting Apache Zookeeper ..."
		exit
	fi
	
	sudo systemctl start kafka
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error starting Apache Kafka ..."
		exit
	fi
	
	sudo systemctl is-active --quiet zookeeper && echo "Apache Zookeeper service is up"
	sudo systemctl is-active --quiet kafka && echo "Apache Kafka service is up"

	sudo mkdir /opt/kafdrop
	cd /opt/kafdrop
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error moving to /opt/kafdrop directory ..."
		exit
	fi
	
	sudo cp ~/khis2-setup/config/templates/config_files/kafdrop-docker-compose.yml docker-compose.yml
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error creating kafdrop docker-compose.yml ..."
		exit
	fi

	sudo docker-compose pull && sudo docker-compose up -d
	if [[ $? != 0 ]]
	then
		tput setaf 1; echo "Error starting Kafdrop ..."
		exit
	fi
}

function passengerselection {
        HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Passenger Installation"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Install Phusion Passenger"
        	 2 "Skip Phusion Passenger installation")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	1)
            		khispassenger;
            		;;
        	2)
            		tput setaf 6 ; echo "Skipping Phusion Passenger installation ..."
        		tput sgr0
            		;;
	esac

}

function khispassenger {
	# Installing Phusion Passenger
	
	# Install our PGP key and add HTTPS support for APT
        sudo apt-get install -y dirmngr gnupg apt-transport-https ca-certificates 

        sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7

        # Add our APT repository
        sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger focal main > /etc/apt/sources.list.d/passenger.list'
        sudo apt-get update

        # Install Passenger + Nginx module
        sudo apt-get install -y libnginx-mod-http-passenger
        
        if [ ! -f /etc/nginx/modules-enabled/50-mod-http-passenger.conf ]; then sudo ln -s /usr/share/nginx/modules-available/mod-http-passenger.load /etc/nginx/modules-enabled/50-mod-http-passenger.conf ; fi
        sudo ls /etc/nginx/conf.d/mod-http-passenger.conf
        
        sudo service nginx restart
	
	sudo /usr/bin/passenger-config validate-install
	
	sudo /usr/sbin/passenger-memory-stats
}

#............................................................................
#............................................................................
# KHIS Installation Procedures
#............................................................................
if ((whoami != kentra)); then
	echo "Please run as KENTRA..."
	exit
fi	

khisstartup;
pythonrequirements;
pythonselection;
dbselection;
khisbootstrap;
khiajava;
khisrvmruby;
khissidekiq;
glassfishselection;
mirthselection;
kafkaselection;
passengerselection;

# Create Kentra  Directory
sudo mkdir /opt/kentra

#............................................................................
#............................................................................