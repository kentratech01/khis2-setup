#!/bin/bash
##################################################################################
##                 								                                ##
## Kentra Technologies®                                                       	##
##   										                                    ##
## KHIS2 installation					                                        ##
##										                                        ##
##################################################################################

DATE=$(date "+%Y%m%d%H%M")
DONE="\e[32mDone\e[0m"
GIT_BRANCH="master"
INSTALL_DIR="khis"
INSTALL_DIR_QC_VER="${INSTALL_DIR:4:4}"
DB_HOST="localhost"
DB_NAME="$INSTALL_DIR"
DB_USER="khis"
DB_PASS="Khis@k3ntr4"
PY_VERSION=py3
KHIS_FOLDER="/opt/kentra"

clear
echo "***********************************************************"
echo "******************* K-HIS GIT INSTALLER *******************"
echo "********************** version 2.0 ************************"
echo "***********************************************************"
echo "**** DEPLOYING ONLY APPLICATION rails and database ********"

function khisvalidateargs {

##### START VALIDATE ARGUMENTS######

resp=$(dialog --backtitle "KHIS2 Installation process" --title "KHIS2 Validate Arguments" \
	--form "\nKHIS2 Configuration" 25 60 16 \
	"GIT_BRANCH:" 1 1 $GIT_BRANCH 1 25 25 30 \
	"INSTALL DIRECTORY:" 2 1 $INSTALL_DIR 2 25 25 30 \
	"DB HOST:" 3 1 $DB_HOST 3 25 25 30 \
	"DB NAME:" 4 1 $DB_NAME 4 25 25 30 \
	"DB USER:" 5 1 $DB_USER 5 25 25 30 \
	"DB PASSWORD:" 6 1 $DB_PASS 6 25 25 30 \
	"KHIS_FOLDER:" 7 1 $KHIS_FOLDER 7 25 25 30 \
	2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	i=0
	while read -r line; do
	   ((i++))
	   declare var$i="${line}"
	done <<< "${resp}"

	GIT_BRANCH="${var1}"
	INSTALL_DIR="${var2}"
	DB_HOST="${var3}"
	DB_NAME="${var4}"
	DB_USER="${var5}"
	DB_PASS="${var6}"
	KHIS_FOLDER="${var7}"

	HEIGHT=15
	WIDTH=40
	CHOICE_HEIGHT=4
	BACKTITLE="KHIS2 - Installation Script"
	TITLE="Python version"
	MENU="Choose one of the following options:"

	OPTIONS=(1 "Python"
        	 2 "Python 3")

	CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

	if [[ $? -eq 1 ]]
	then
		clear
		tput setaf 1; echo "Abborting KHIS installation ..."
		exit
	fi

	clear

	case $CHOICE in
        	1)
            		PY_VERSION=py
            		;;
        	2)
            		PY_VERSION=py3
            		;;
	esac

	echo -e "" 
	echo -e "\e[1m\e[96mDirectory:\e[0m $KHIS_FOLDER/$INSTALL_DIR"
	echo -e "\e[1m\e[96mVersion: \e[0m\e[43m $GIT_BRANCH \e[0m"
	echo -e "\e[1m\e[96mDB Host:\e[0m $DB_HOST"
	echo -e "\e[1m\e[96mDB Name: \e[0m\e[43m $DB_NAME \e[0m"
	echo -e "\e[1m\e[96mDB User:\e[0m $DB_USER"
	echo -e "\e[1m\e[96mDB Pass: \e[0m\e[43m $DB_PASS \e[0m"
	echo -e "\e[1m\e[96mPython Version: \e[0m\e[43m $PY_VERSION \e[0m"
	echo ""

	##### END VALIDATE ARGUMENTS######

	echo -e "\e[93mDo you confirm the installation data?\e[0m"

  	select result in Yes No
	do
    	case $result in
        	Yes )
				break;;
        	No ) 
			exit;
    	esac
	done

	echo ""
	echo -e "\e[93mCreating Installation Environment Diretory (/FILES/INSTALLATION/$INSTALL_DIR/$DATE\e[0m"
	sudo mkdir -p ~/FILES/INSTALLATION/$INSTALL_DIR/$DATE
	echo -e "\e[93mCreating Backup Environment Diretory (/FILES/BACKUP/$INSTALL_DIR/$DATE\e[0m"
	sudo mkdir -p ~/FILES/BACKUPS/$INSTALL_DIR
	echo "Copy database.yml config..."
	sudo cp $KHIS_FOLDER/$INSTALL_DIR/config/database.yml ~/FILES/BACKUPS/$INSTALL_DIR

	echo ""
	echo -e "\e[93mChecking disk space\e[0m"
	df -h

	echo ""
	echo -e "\e[93mDo you wish to copy properties files for restore after upgrade?\e[0m"

  	select result in Yes No
	do
		case $result in
        	Yes )
				echo ""
				echo -e "\e[93mRemove previous backups\e[0m"
				sudo rm -R `ls -1 -d ~/FILES/BACKUPS/$INSTALL_DIR/*/`

				echo -e "\e[1m\e[96m########## Copy properties files do restore after upgrade ##########\e[0m"
				echo ""
				sudo cp -R $KHIS_FOLDER/$INSTALL_DIR ~/FILES/BACKUPS/$INSTALL_DIR/$DATE/

				echo "Copy Properties Application ..."
				sudo cp $KHIS_FOLDER/$INSTALL_DIR/config/database.yml ~/FILES/BACKUPS/$INSTALL_DIR

				sudo cp $KHIS_FOLDER/$INSTALL_DIR/Gemfile.lock ~/FILES/BACKUPS/$INSTALL_DIR
				sudo cp $KHIS_FOLDER/$INSTALL_DIR/package-lock.json ~/FILES/BACKUPS/$INSTALL_DIR
				sudo cp $KHIS_FOLDER/$INSTALL_DIR/yarn.lock ~/FILES/BACKUPS/$INSTALL_DIR
				sudo cp -R $KHIS_FOLDER/$INSTALL_DIR/node_modules ~/FILES/BACKUPS/$INSTALL_DIR/node_modules

				echo -e $DONE

				break;;
        	No ) 
			echo "Properties files not copy. please check you properties after the upgrade..."
			break;;
    	esac
	done

}

function khissources {

	echo ""
	echo -e "\e[93mChecking disk space\e[0m"
	df -h

	echo ""
	echo -e "\e[93mCDo you wish to get new sources?\e[0m"

  	select result in Yes No
	do
    	case $result in
        	Yes )
				echo ""
				echo -e "" 
				sudo mkdir -p ~/FILES/INSTALLATION/$INSTALL_DIR
				sudo chmod 777 -R ~/FILES/INSTALLATION/$INSTALL_DIR
				cd ~/FILES/INSTALLATION/$INSTALL_DIR
				sudo rm -rf *

				echo "Get HIS SOURCES ..." # Novo repositório
					echo -e $GIT_BRANCH
				    git clone --single-branch --branch $GIT_BRANCH "git@bitbucket.org:kentratech01/khis.git" || { echo 'git clone failed' ; exit 1; }
				    

				break;;
        	No ) 
			echo "Sources not Getted, please CHECK if you have the RIGHT SOURCES for this upgrade BEFORE CONTINUE..."
			break;;
    	esac
	done

	sed -i "s/database:.*/database:  ${DB_NAME}/" ~/FILES/INSTALLATION/$INSTALL_DIR/khis/config/database.yml
	sed -i "s/username:.*/username:  ${DB_USER}/" ~/FILES/INSTALLATION/$INSTALL_DIR/khis/config/database.yml
	sed -i "s/password:.*/password: ${DB_PASS}/" ~/FILES/INSTALLATION/$INSTALL_DIR/khis/config/database.yml
	sed -i "s/host:.*/host: ${DB_HOST}/" ~/FILES/INSTALLATION/$INSTALL_DIR/khis/config/database.yml

	echo ""
	echo -e "\e[93mChecking disk space\e[0m"
	df -h

	echo ""
	echo -e "\e[93mIntend to proceed with the upgrade?\e[0m"

	select result in Yes No
	do
	    case $result in
	        Yes ) 
				break;;
	        No ) 
				exit 1;
	    esac
	done
}

function khisbackup {
	echo ""
	echo -e "\e[1m\e[96m########## Do you want a database backup ? ##########\e[0m"
	echo ""

	select result in Yes No
	do
	    case $result in
	        Yes ) 
				echo -e "\e[93mBackup database previous version...\e[0m"
				sudo pg_dump --host localhost --port 5432 --username khis --password --format custom --blobs --verbose --compress 9 --file ~/FILES/BACKUPS/$INSTALL_DIR/$DATE/db_backup.backup $INSTALL_DIR
				echo -e $DONE
				break;;
	        No ) 
				echo -e "\e[93mSkipping database backup\e[0m"
				break;;
	    esac
	done
}

function khisappinstall {

	echo ""
	echo -e "\e[1m\e[96m########## Copy Files from Repository ? ##########\e[0m"
	echo ""
	echo -e "\e[93mInstall KHIS APPlication?\e[0m"
	select result in Yes No
	do
	    case $result in
	        Yes ) 		
			echo "Get UX Version..."
			#cd ~/FILES/INSTALLATION/khis
			#svn up

			cd ~/FILES/INSTALLATION/$INSTALL_DIR/
			
			echo ""
			echo "Delete unused files ...."
			sudo find ~/FILES/INSTALLATION/$INSTALL_DIR/khis/. -name "*.project" -exec rm -rf {} \;
			sudo find ~/FILES/INSTALLATION/$INSTALL_DIR/khis/. -name "*.svn" -exec rm -rf {} \;

			sudo rm ~/FILES/INSTALLATION/$INSTALL_DIR/khis/Gemfile.lock
			sudo rm ~/FILES/INSTALLATION/$INSTALL_DIR/khis/package-lock.json
			sudo rm ~/FILES/INSTALLATION/$INSTALL_DIR/khis/yarn.lock

			echo ""	
			echo "UnInstall Previous KHIS Version..."
			sudo rm -r $KHIS_FOLDER/$INSTALL_DIR

			echo ""	
			echo "Install KHIS Version..."
			#create khis destination
			sudo mkdir -p $KHIS_FOLDER/$INSTALL_DIR
			#send khis new version to destination
			sudo cp -rf ~/FILES/INSTALLATION/$INSTALL_DIR/khis/* $KHIS_FOLDER/$INSTALL_DIR
			echo -e $DONE

			echo ""
			
			#echo "Restoring database.yml"
			#sudo cp ~/FILES/BACKUPS/$INSTALL_DIR/database.yml $KHIS_FOLDER/$INSTALL_DIR/config/

			echo ""
			echo -e "\e[93mGo to Install dir...\e[0m"
			cd $KHIS_FOLDER/$INSTALL_DIR

			sudo chmod 777 -R $KHIS_FOLDER/$INSTALL_DIR

			echo ""
			echo -e "\e[93mInstall Gems (A Internet Connection is needned) \e[0m"
			bundle install || { echo 'bundle install test failed' ; exit 1; }

			echo ""
			echo "Precompile production rake enviroment..."
			RAILS_ENV=production bundle exec rake assets:precompile || { echo 'RAILS_ENV=production bundle exec rake assets:precompile failed' ; exit 1; }
			echo -e $DONE
			
			echo ""
			echo "Precompile production webpack enviroment..."
			RAILS_ENV=production RACK_ENV=production NODE_ENV=production bin/webpack || { echo 'RAILS_ENV=production RACK_ENV=production NODE_ENV=production bin/webpack failed' ; exit 1; }
			echo -e $DONE

			sudo cat ~/khis2-setup/config/templates/config_files/production.rb > change.txt && mv change.txt $KHIS_FOLDER/$INSTALL_DIR/config/environments/production.rb

			sudo cat ~/khis2-setup/config/templates/config_files/puma.rb > change.txt && sudo mv change.txt $KHIS_FOLDER/$INSTALL_DIR/config/puma.rb
	        if [[ $? != 0 ]]
	        then
	                tput setaf 1; echo "Error changing sites-available ,,,"
	                exit
	        fi

	        chmod +x /home/kentra/khis2-setup/config/templates/cleanup/log_khis_cleanup.sh

	        chmod +x /home/kentra/khis2-setup/config/templates/cleanup/log_kafka_cleanup.sh

	        crontab -l | egrep "log_khis_cleanup" || sudo crontab -u kentra -l | { cat; echo "0 0  *  *  * /home/kentra/khis2-setup/config/templates/cleanup/log_khis_cleanup.sh"; } | sudo crontab -u kentra -

			sudo crontab -u kafka -l | egrep "log_kafka_cleanup" || sudo crontab -u kafka -l | { cat; echo "0 0  *  *  * /home/kentra/khis2-setup/config/templates/cleanup/log_kafka_cleanup.sh"; } | sudo crontab -u kafka -

	        mkdir -p shared/pids shared/sockets shared/log
			echo ""
			echo "Restart application..."
			lsof -ti:3000 | xargs kill -9
			bundle exec rails s -e production -d
			echo -e $DONE

			echo -e "\e[1mKHIS application deploy done."

			break;;

			No )
			echo "Skipping"
			break;;

	    esac

	done
}

function khisexecutemigrations {
	echo ""
	echo -e "\e[93mExecute migrations? \e[0m"
	select result in Yes No
	do
	    case $result in
	        Yes ) 

			echo "Executing db migrations..."
			RAILS_ENV=production bundle exec rake db:migrate || { echo 'RAILS_ENV=production bundle exec rake db:migrate failed' ; exit 1; }
			echo -e $DONE
			break;;

	        No ) 
			echo "Skipping"
			break;;
	    esac
	done
}

function khisexecutedb {

	echo ""
	echo -e "\e[93mExecute backend db functions? \e[0m"
	select result in Yes No
	do
	    case $result in
	        Yes ) 

			echo "Get DB Version..."
			cd $KHIS_FOLDER/$INSTALL_DIR/db/backend/

			case $PY_VERSION in
				py )
					python installer_functions.py $DB_HOST $DB_NAME $DB_USER $DB_PASS
					break;;
				py2 )
					python2 installer_functions.py $DB_HOST $DB_NAME $DB_USER $DB_PASS
					break;;
				py3 )
					python3 installer_functions.py $DB_HOST $DB_NAME $DB_USER $DB_PASS
					break;;
			esac

			break;;

	        No ) 
			echo "Skipping"
			break;;
	    esac
	done
}

function khisexecuterunscripts {
	
	echo ""
	echo -e "\e[93mDo you wish to run version scripts? \e[0m"

	select result in Yes No
	do
	    case $result in
	        Yes )
			while true; do

				cd $KHIS_FOLDER/$INSTALL_DIR/db/backend/

			    read -p "Environment for version label: " version_label
				if [[ ! -z $version_label ]]; then
					case $PY_VERSION in
						py )
							python installer_version.py $DB_HOST $DB_NAME $DB_USER $DB_PASS $version_label
							break;;
						py2 )
							python2 installer_version.py $DB_HOST $DB_NAME $DB_USER $DB_PASS $version_label
							break;;
						py3 )
							python3 installer_version.py $DB_HOST $DB_NAME $DB_USER $DB_PASS $version_label
							break;;
					esac

					break
				fi
			done
			break;;
			
	        No ) 
			"Not execute version script...."
			break;;
	    esac
	done


	echo ""
	echo -e "\e[93mDo you wish delete cloned sources? \e[0m"

	select result in Yes No
	do
	    case $result in
	        Yes )
			echo ""
			echo "deleting sources..."
			cd ~/FILES/INSTALLATION/$INSTALL_DIR
			sudo rm -rf *
			break;;
			
	        No ) 
			"Not deleting sources...."
			break;;
	    esac
	done

	echo "K-HIS deploy done!"
}

function khisexecutesyncusers {
	echo ""
	echo -e "\e[93mSynchronize users with the SSO (Keycloak)? \e[0m"
	select result in Yes No
	do
	    case $result in
	        Yes ) 
			RAILS_ENV=production bundle exec rake sso:sync_users || { echo 'RAILS_ENV=production bundle exec rake sso:sync_users' ; exit 1; }
			echo -e $DONE
			break;;

	        No ) 
			echo "Skipping"
			break;;
	    esac
	done
}

#............................................................................
#............................................................................
# KHIS Installation Procedures
#............................................................................
if ((whoami != kentra)); then
	echo "Please run as KENTRA..."
	exit
fi	

khisvalidateargs;
khissources;
khisbackup;
khisappinstall;
khisexecutemigrations;
khisexecutedb;
khisexecuterunscripts;
khisexecutesyncusers;

#............................................................................
#............................................................................
